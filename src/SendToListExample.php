<?php
/**
 * Example script that sends the message 'Hello World!' to 1 number with the recipient TEST
 */
require __DIR__ . '/bootstrap.php';

use Buzz\Browser;
use Buzz\Client\ClientInterface;
use Buzz\Client\Curl;

# prepare data

$url = API_URL . '/message.json?apikey=' . API_KEY;
$headers = array(
    'Content-Type' => 'application/json',
);
$payload = array(
	'message' => array(
	    'sender' 		=> 'TEST',
	    'recipients'	=> '1069', # TODO: The list of the id you want to send to
	    'message'	 	=> 'Hello World!'
	)
);
$payload_encoded = json_encode($payload);

# call api

$browser = new Browser(new Curl());
$browser->getClient()->setTimeout(120);
$result = $browser->post($url, $headers, $payload_encoded);

# handle result
$json = json_decode($result->getContent());

if ($result->getStatusCode() != 201) {
    echo 'ERROR: got status code: ' . $result->getStatusCode() . ' ' . $json->message . PHP_EOL;
}

var_dump($json);
