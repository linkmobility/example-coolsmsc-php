<?php
// basic setup
date_default_timezone_set('Europe/Copenhagen');
ini_set("display_errors", 1);
error_reporting(E_ALL ^ E_NOTICE);

// composer autoload
require_once __DIR__ . '/../vendor/autoload.php';

// config options
define('API_URL', 'https://api.linkmobility.dk/v2');
define('API_KEY', 'change-your-api-key');
