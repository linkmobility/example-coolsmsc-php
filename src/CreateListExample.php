<?php
/**
 * Example script that creates a list
 */
require __DIR__ . '/bootstrap.php';

use Buzz\Browser;
use Buzz\Client\ClientInterface;
use Buzz\Client\Curl;

# prepare data

$url = API_URL . '/list.json?apikey=' . API_KEY;
$headers = array(
    'Content-Type' => 'application/json',
);
$payload = array(
	'list' => array(
	    'parent' 	=> 0,
	    'name'		=> 'EXAMPLE LIST'
	)
);
$payload_encoded = json_encode($payload);

# call api

$browser = new Browser(new Curl());
$browser->getClient()->setTimeout(120);
$result = $browser->post($url, $headers, $payload_encoded);

# handle result
$json = json_decode($result->getContent());

if ($result->getStatusCode() != 201) {
    echo 'ERROR: got status code: ' . $result->getStatusCode() . ' ' . $json->message . PHP_EOL;
} else {
	echo 'You just created the list ' . $json->list->name . ' and it got id ' . $json->list->id . PHP_EOL;
}
