<?php
/**
 * More advanced example that imports a list from a csv file, sends a test message to the list
 *
 * Notice :
 *  This example uses the mergefields Name and Number so you need to have them already
 *  It also uses and existing list, so you need to change that id
 */
require __DIR__ . '/bootstrap.php';

# TODO: The list of the id you want to send to
$listid = 1069;

use Buzz\Browser;
use Buzz\Client\ClientInterface;
use Buzz\Client\Curl;
$browser = new Browser(new Curl());
$browser->getClient()->setTimeout(120);

$headers = array(
    'Content-Type' => 'application/json',
);

# create import job
$url = API_URL . '/import.json?apikey=' . API_KEY;
$payload = array(
	'import' => array(
	    'listid' 		=> $listid,
	    'name'			=> 'EXAMPLE IMPORT',
	    'clean'	 		=> 1,
	    'duplicate'	 	=> 0
	)
);
$payload_encoded = json_encode($payload);

$result = $browser->post($url, $headers, $payload_encoded);
$json = json_decode($result->getContent());

if ($result->getStatusCode() != 201) {
    echo 'ERROR: got status code: ' . $result->getStatusCode() . ' ' . $json->message;
    exit(1);
}

$hash = $json->import->hash;
echo 'Import hash created ' . $hash . PHP_EOL;

# load contacts from a csv file and add them to the list

$url = API_URL . '/import/' . $hash . '.json?apikey=' . API_KEY;
$payload = [
	'import' => [
		'keys' => [ 'Mobile', 'Name', 'Number' ],
		'values' => [
			[ '+4512721272', 'Jack Bauer', '1' ],
			[ '+4512345678', 'Jack Sparrow', '2' ],
			[ '+4512721272', 'Chuck Norris', '3' ],
			[ '+4587654321', 'Some One', '4' ]
		]
	]
];
$payload_encoded = json_encode($payload);

$result = $browser->put($url, $headers, $payload_encoded);
$json = json_decode($result->getContent());

if ($result->getStatusCode() != 200) {
    echo 'ERROR: got status code: ' . $result->getStatusCode() . ' ' . $json->message;
    exit(1);
}

echo 'Imported ' . $json->import->total . PHP_EOL;

# send a message to the list and use filtering

$url = API_URL . '/message.json?apikey=' . API_KEY;
$payload = array(
	'message' => array(
	    'sender' 		=> 'TEST',
	    'recipients'	=> $listid,
	    'message'	 	=> 'Hello World!',
	    'filter'		=> [
	    	'match' 	=> 'any',
	    	'rules' 	=> [
	    		[
	    			'field' 	=> 'Name',
	    			'operation' => 'contains',
	    			'value' 	=> 'Jack'
	    		]
	    	]
	    ]
	)
);
$payload_encoded = json_encode($payload);

# call api
$result = $browser->post($url, $headers, $payload_encoded);

# handle result
$json = json_decode($result->getContent());

if ($result->getStatusCode() != 201) {
    echo 'ERROR: got status code: ' . $result->getStatusCode() . ' ' . $json->message . PHP_EOL;
}

var_dump($json);
