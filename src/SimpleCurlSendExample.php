<?php
/**
 * Simple example script that only uses curl and sends the message 'Hello World!' to 1 number with the sender id TEST
 *
 * the example only uses the bootstrap for config options
 */

require __DIR__ . '/bootstrap.php';

# prepare data

$url = API_URL . '/message.json?apikey=' . API_KEY;
$headers = array(
    'Content-Type: application/json'
);
$payload = array(
    'message' => array(
        'sender'        => 'TEST',
        'recipients'    => '+4512721272',
        'message'       => 'Hello World!'
    )
);
$payload_encoded = json_encode($payload);

# call api

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL               => $url,
  CURLOPT_RETURNTRANSFER    => true,
  CURLOPT_TIMEOUT           => 30,
  CURLOPT_HTTP_VERSION      => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST     => 'POST',
  CURLOPT_POSTFIELDS        => $payload_encoded,
  CURLOPT_HTTPHEADER        => $headers
));

$result = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

# handle result

$json = json_decode($result);

if ($err || $json->status != 201) {
    echo 'ERROR: got status code: ' . $json->status . ' ' . $json->message . PHP_EOL;
}

var_dump($json);
