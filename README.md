This project holds a collection of different examples using the rest coolsms rest api

The examples uses a simple rest client called Buzz for convienience

## Installation

```
# install composer
curl -sS https://getcomposer.org/installer | php

# install the packages
php composer.phar install
```

## Test scripts

All the test scripts are located in the src folder and they should have a descriptive name identifying what it does

Go into src/bootstrap.php and add your apikey